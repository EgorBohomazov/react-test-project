import {
  ADD_INVOICE,
  DELETE_INVOICE,
  SET_INVOICE,
  GET_INVOICES,
  SET_INVOICE_CHECKED,
  CHECK_ALL_INVOICES,
} from "../actions/actionTypes";

const initialState = [];

const invoicesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_INVOICE:
      return [
        ...state, {
          id: action.value.id,
          customer_id: action.value.customer_id,
          discount: action.value.discount,
          total: action.value.total,
          createdAt: action.value.createdAt
        }
      ];
    case GET_INVOICES:
      return action.value.map(elem => ({ ...elem, checked: false }));
    case SET_INVOICE: {
      return state.map(elem => ((elem.id === action.value.id)
        ? { ...elem,
          customer_id: action.value.customer_id,
          discount: action.value.discount,
          total: action.value.total
        }
        : elem));
    }
    case SET_INVOICE_CHECKED:
      return state.map((elem) => {
        if (elem.id === action.value) {
          return { ...elem, checked: !elem.checked };
        }
        return elem;
      });
    case CHECK_ALL_INVOICES:
      return state.map(elem => ({ ...elem, checked: action.value }));
    case DELETE_INVOICE:
      return state.filter(elem => elem.id !== action.value);
    // case SORT_BY_TOTAL: {
    //   console.log(state);
    //   return state.sort((a, b) => b.total - a.total);
    // }
    default:
      return state;
  }
};


export default invoicesReducer;
