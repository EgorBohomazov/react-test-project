import todoReducer from "./todos";
import customersReducer from "./customers";
import productsReducer from "./products";
import invoicesReducer from "./invoices";
import invoiceItemsReducer from "./invoiceItems";
import usersReducer from "./users";

export default { todoReducer, customersReducer, productsReducer, invoicesReducer, invoiceItemsReducer, usersReducer };
