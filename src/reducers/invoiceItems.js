import {
  ADD_INVOICE_ITEM,
  DELETE_INVOICE_ITEM_LOCAL,
  GET_INVOICE_ITEMS,
  SET_QUANTITY_DEC,
  SET_QUANTITY_INC,
  ADD_ITEM_PRICES,
  ADD_INVOICE_ITEM_LOCAL,
  SET_ITEM_CHECKED,
  SET_ITEMS_CHECKED_ALL, DELETE_CHECKED_ITEMS, CLEAR_INVOICE_ITEMS
} from "../actions/actionTypes";

const initialState = [];

const invoiceItemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_INVOICE_ITEM: {
      return [
        ...state, {
          id: action.value.id,
          invoice_id: action.value.invoice_id,
          product_id: action.value.product_id,
          quantity: action.value.quantity,
          checked: false
        }
      ];
    }
    case ADD_INVOICE_ITEM_LOCAL: {
      const checkSame = state.some(elem => elem.product_id === action.value.product_id);

      if (checkSame === false) {
        return [
          ...state, {
            invoice_id: action.value.invoice_id,
            product_id: action.value.product_id,
            quantity: action.value.quantity,
            price: action.value.price,
            checked: false
          }
        ];
      }
      return state.map((elem) => {
        if (elem.product_id === action.value.product_id) {
          return { ...elem, quantity: elem.quantity + action.value.quantity, price: elem.price + action.value.price };
        }
        return elem;
      });
    }
    case DELETE_INVOICE_ITEM_LOCAL:
      return state.filter(elem => elem.product_id !== action.value);
    case GET_INVOICE_ITEMS:
      return action.value;
    case ADD_ITEM_PRICES:
      return state.map((elem) => {
        if (elem.product_id === action.value.id) {
          return { ...elem, price: elem.quantity * action.value.price.toFixed(2), checked: false };
        }
        return elem;
      });
    case SET_QUANTITY_INC:
      return state.map((elem) => {
        if (elem.product_id === action.value) {
          return ({ ...elem,
            quantity: elem.quantity + 1,
            price: elem.price + (elem.price / elem.quantity) });
        }
        return elem;
      });
    case SET_QUANTITY_DEC:
      return state.map((elem) => {
        if (elem.product_id === action.value) {
          return ({ ...elem,
            quantity: elem.quantity - 1,
            price: elem.price - (elem.price / elem.quantity) });
        }
        return elem;
      });
    case SET_ITEM_CHECKED:
      return state.map((elem) => {
        if (elem.product_id === action.value) {
          return ({ ...elem, checked: !elem.checked });
        }
        return elem;
      });
    case SET_ITEMS_CHECKED_ALL:
      return state.map(elem => ({ ...elem, checked: action.value }));
    case DELETE_CHECKED_ITEMS:
      return state.filter(elem => elem.checked === false);
    case CLEAR_INVOICE_ITEMS:
      return initialState;
    default:
      return state;
  }
};


export default invoiceItemsReducer;
