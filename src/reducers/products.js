import {
  ADD_PRODUCT,
  DELETE_PRODUCT,
  SET_PRODUCT_CHECKED,
  CHECK_ALL_PRODUCTS,
  DELETE_CHECKED_PRODUCTS,
  SET_PRODUCT,
  GET_PRODUCTS
} from "../actions/actionTypes";

const initialState = [];

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT: {
      return [
        ...state, {
          id: action.value.id,
          name: action.value.name,
          price: action.value.price,
          checked: false
        }
      ];
    }
    case DELETE_PRODUCT: {
      return state.filter(elem => elem.id !== action.value);
    }
    case SET_PRODUCT_CHECKED:
      return state.map(elem => ((elem.id === action.value)
        ? { ...elem, checked: !elem.checked }
        : elem));
    case CHECK_ALL_PRODUCTS:
      return state.map(elem => ({ ...elem, checked: action.value }));
    case DELETE_CHECKED_PRODUCTS:
      return state.filter(elem => elem.checked === false);
    case SET_PRODUCT: {
      return state.map(elem => ((elem.id === action.value.id)
        ? { ...elem, name: action.value.name, price: action.value.price }
        : elem)); }
    case GET_PRODUCTS:
      return action.value;
    default:
      return state;
  }
};


export default productsReducer;
