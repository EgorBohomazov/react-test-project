import {
  ADD_TODO,
  DELETE_TODO,
  CHANGE_TODO_STATUS,
  CHANGE_TODO_NAME,
  DELETE_ALL_DONE,
  COMPLETE_ALL_TASKS } from "../actions/actionTypes";

const initialState = [];

const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state, {
          id: action.value.id,
          text: action.value.text,
          status: action.value.status
        }
      ];
    case DELETE_TODO:
      return state.filter(elem => elem.id !== action.value);
    case CHANGE_TODO_STATUS:
      return state.map(elem => ((elem.id === action.value)
        ? { ...elem, status: !elem.status }
        : elem));
    case CHANGE_TODO_NAME:
      return state.map(elem => ((elem.id === action.value.id)
        ? { ...elem, text: action.value.text }
        : elem));
    case DELETE_ALL_DONE:
      return state.filter(elem => elem.status === false);
    case COMPLETE_ALL_TASKS: {
      if (state.every(elem => elem.status === true)) {
        return state.map(todo => ({ ...todo, status: false }));
      }

      return state.map(todo => ({ ...todo, status: true }));
    }
    default:
      return state;
  }
};


export default todoReducer;
