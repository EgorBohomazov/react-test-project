import {
  LOG_IN,
  LOG_OUT,
  CREATE_NEW_USER,
  UPLOAD_IMAGE,
  UPDATE_USERS_DATA
} from "../actions/actionTypes";

const initialState = {
  isLogIn: false,
  firstName: "",
  lastName: "",
  age: "",
  address: "",
  phone: "",
  login: "",
  password: "",
  image: ""
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_NEW_USER:
      return {
        ...state,
        isLogIn: true,
        id: action.value.id,
        firstName: action.value.firstName,
        lastName: action.value.lastName,
        age: action.value.age,
        address: action.value.address,
        login: action.value.login
      };
    case LOG_IN:
      if (action.value) {
        return {
          ...state,
          isLogIn: true,
          id: action.value.id,
          firstName: action.value.firstName,
          lastName: action.value.lastName,
          age: action.value.age,
          address: action.value.address,
          login: action.value.login,
          phone: action.value.phone
        };
      }
      return initialState;
    case LOG_OUT:
      return initialState;
    case UPLOAD_IMAGE:
      return { ...state, image: action.value };
    default:
      return state;
  }
};


export default usersReducer;
