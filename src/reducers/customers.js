import {
  ADD_USER,
  SET_USER,
  DELETE_USER,
  SET_CHECKED,
  CHECK_ALL,
  DELETE_CHECKED,
  GET_CUSTOMERS
} from "../actions/actionTypes";

const initialState = [];

const customersReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_USER:
      return [
        ...state, {
          id: action.value.id,
          name: action.value.name,
          address: action.value.address,
          phone: action.value.phone,
          checked: false
        }
      ];
    case DELETE_USER:
      return state.filter(elem => elem.id !== action.value);
    case SET_USER:
      return state.map(elem => ((elem.id === action.value.id)
        ? { ...elem, name: action.value.name, address: action.value.address, phone: action.value.phone }
        : elem));
    case SET_CHECKED:
      return state.map(elem => ((elem.id === action.value)
        ? { ...elem, checked: !elem.checked }
        : elem));
    case CHECK_ALL: {
      if (state.every(elem => elem.checked === true)) {
        return state.map(user => ({ ...user, checked: false }));
      }

      return state.map(user => ({ ...user, checked: action.value }));
    }
    case DELETE_CHECKED:
      return state.filter(user => user.checked === false);
    case GET_CUSTOMERS:
      return action.value;
    default:
      return state;
  }
};


export default customersReducer;
