import {
  ADD_USER,
  DELETE_USER,
  SET_USER,
  SET_CHECKED,
  CHECK_ALL,
  GET_CUSTOMERS
} from "./actionTypes";
import { get, post, put, remove } from "../../utils/api";
import urls from "../../utils/url";

export const addUser = item => (dispatch) => {
  post(urls.customers(), item).then(res => dispatch({
    type: ADD_USER,
    value: res
  }));
};

export const deleteUser = id => (dispatch) => {
  remove(urls.customers(id)).then(res => dispatch({
    type: DELETE_USER,
    value: res.id
  }));
};

export const setUser = data => (dispatch) => {
  put(urls.customers(data.id), data).then(res => dispatch({
    type: SET_USER,
    value: res
  }));
};

export const setChecked = value => ({
  type: SET_CHECKED,
  value
});

export const checkAll = value => (
  {
    type: CHECK_ALL,
    value
  }
);

export const deleteChecked = customers => (dispatch) => {
  const promises = [];

  customers.forEach((item) => {
    if (item.checked === true) {
      promises.push(remove(urls.customers(item.id)).then((res) => {
        dispatch({
          type: DELETE_USER,
          value: res.id
        });
      }));
    }
  });

  Promise.all(promises);
};


export const getCustomers = () => dispatch => new Promise((resolve) => {
  get(urls.customers()).then((res) => {
    dispatch({
      type: GET_CUSTOMERS,
      value: res
    });

    resolve();
  });
});
