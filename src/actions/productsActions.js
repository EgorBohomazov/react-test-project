import {
  ADD_PRODUCT,
  DELETE_PRODUCT,
  SET_PRODUCT_CHECKED,
  CHECK_ALL_PRODUCTS,
  DELETE_CHECKED_PRODUCTS,
  SET_PRODUCT,
  GET_PRODUCTS } from "./actionTypes";
import { get, post, remove, put } from "../../utils/api";
import urls from "../../utils/url";


export const addProduct = item => (dispatch) => {
  post(urls.products(), item).then(res => dispatch({
    type: ADD_PRODUCT,
    value: res
  }));
};

export const deleteProduct = id => (dispatch) => {
  remove(urls.products(id)).then(res => dispatch({
    type: DELETE_PRODUCT,
    value: res.id
  }));
};

export const setProductChecked = value => ({
  type: SET_PRODUCT_CHECKED,
  value
});

export const checkAllProducts = value => ({
  type: CHECK_ALL_PRODUCTS,
  value
});

export const deleteCheckedProducts = items => (dispatch) => {
  const promises = [];

  items.forEach((item) => {
    if (item.checked === true) {
      promises.push(remove(urls.products(item.id)).then((res) => {
        dispatch({
          type: DELETE_PRODUCT,
          value: res.id
        });
      }));
    }
  });

  Promise.all(promises);
};

export const setProduct = data => (dispatch) => {
  put(urls.products(data.id), data).then(res => dispatch({
    type: SET_PRODUCT,
    value: res
  }));
};

export const getProducts = () => dispatch => new Promise((resolve) => {
  get(urls.products()).then(res => dispatch({
    type: GET_PRODUCTS,
    value: res
  }));
  resolve();
});
