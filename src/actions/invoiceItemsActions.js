import {
  ADD_INVOICE_ITEM,
  DELETE_INVOICE_ITEM_LOCAL,
  GET_INVOICE_ITEMS,
  SET_QUANTITY_INC,
  SET_QUANTITY_DEC,
  ADD_ITEM_PRICES,
  ADD_INVOICE_ITEM_LOCAL,
  SET_ITEM_CHECKED,
  SET_ITEMS_CHECKED_ALL,
  DELETE_CHECKED_ITEMS,
  CLEAR_INVOICE_ITEMS
} from "./actionTypes";
import { post, get } from "../../utils/api";
import urls from "../../utils/url";

export const addInvoiceItem = (invoiceId, item) => (dispatch) => {
  post(urls.invoiceItems(invoiceId), item).then(res => dispatch({
    type: ADD_INVOICE_ITEM,
    value: res
  }));
};

export const addInvoiceItemLocal = value => ({
  type: ADD_INVOICE_ITEM_LOCAL,
  value
});


export const deleteInvoiceItemLocal = value => ({
  type: DELETE_INVOICE_ITEM_LOCAL,
  value
});


export const getInvoiceItems = invoiceId => dispatch => new Promise((resolve) => {
  get(urls.invoiceItems(invoiceId)).then((res) => {
    dispatch({
      type: GET_INVOICE_ITEMS,
      value: res
    });

    resolve();
  });
});

export const getAllInvoiceItems = () => dispatch => new Promise((resolve) => {
  const promises = [];
  const invoiceItems = [];

  get(urls.invoices()).then((response) => {
    response.forEach((item) => {
      promises.push(get(urls.invoiceItems(item.id)).then((res) => {
        res.forEach((invoiceItem) => {
          invoiceItems.push(invoiceItem);
          dispatch({
            type: ADD_INVOICE_ITEM_LOCAL,
            value: invoiceItem
          });
        });
      }));
    });
  });
  Promise.all(promises).finally(() => { resolve(invoiceItems); });
});

export const setItemsCheckedAll = value => ({
  type: SET_ITEMS_CHECKED_ALL,
  value
});

export const deleteCheckedItems = () => ({
  type: DELETE_CHECKED_ITEMS
});

export const setQuantityInc = value => ({
  type: SET_QUANTITY_INC,
  value
});

export const setItemChecked = value => ({
  type: SET_ITEM_CHECKED,
  value
});

export const setQuantityDec = value => ({
  type: SET_QUANTITY_DEC,
  value
});

export const addItemPrices = value => ({
  type: ADD_ITEM_PRICES,
  value
});

export const clearInvoiceItems = () => ({
  type: CLEAR_INVOICE_ITEMS
});
