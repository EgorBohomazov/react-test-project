import {
  ADD_INVOICE,
  DELETE_INVOICE,
  SET_INVOICE_CHECKED,
  SET_INVOICE,
  CHECK_ALL_INVOICES,
  GET_INVOICES,
  SORT_BY_TOTAL,
  SORT_BY_TOTAL_REVERSE
} from "./actionTypes";
import { get, post, remove, put } from "../../utils/api";
import urls from "../../utils/url";


// export const addInvoice = () => dispatch => new Promise((resolve) => {
//   get(urls.customers()).then((res) => {
//     dispatch({
//       type: ADD_INVOICE,
//       value: res
//     });
//
//     resolve();
//   });
// });

export const addInvoice = (item, invoiceItems) => (dispatch) => {
  post(urls.invoices(), item).then((res) => {
    dispatch({
      type: ADD_INVOICE,
      value: res
    });

    const promises = [];

    invoiceItems.forEach((invoiceItem) => {
      invoiceItem.invoice_id = res.id;
      promises.push(post(urls.invoiceItems(res.id), invoiceItem));
    });

    Promise.all(promises);
  });
};

export const deleteInvoicesChecked = invoices => (dispatch) => {
  const promises = [];
  const itemsPromises = [];

  invoices.forEach((invoice) => {
    if (invoice.checked === true) {
      promises.push(remove(urls.invoices(invoice.id)).then((res) => {
        dispatch({
          type: DELETE_INVOICE,
          value: res.id
        });
      }));
      get(urls.invoiceItems(invoice.id)).then((res) => {
        res.forEach((invoiceItem) => {
          itemsPromises.push(remove(urls.invoiceItems(invoice.id, invoiceItem.id)));
        });
      });
    }
  });

  Promise.all(promises);
  Promise.all(itemsPromises);
};

export const deleteInvoice = id => (dispatch) => {
  remove(urls.invoices(id)).then((res) => {
    dispatch({
      type: DELETE_INVOICE,
      value: res.id
    });

    const promises = [];

    get(urls.invoiceItems(id)).then((res) => {
      res.forEach((item) => {
        promises.push(remove(urls.invoiceItems(id, item.id)));
      });
    });
    Promise.all(promises);
  });
};

export const setInvoiceChecked = value => ({
  type: SET_INVOICE_CHECKED,
  value
});

export const checkAllInvoices = value => ({
  type: CHECK_ALL_INVOICES,
  value
});

export const setInvoice = (invoice, invoiceItems) => (dispatch) => {
  put(urls.invoices(invoice.id),
    {
      id: invoice.id,
      customer_id: invoice.customer_id,
      total: invoice.total,
      discount: invoice.discount }).then(res => dispatch({
    type: SET_INVOICE,
    value: res
  })).then(() => {
    get(urls.invoiceItems(invoice.id)).then((res) => {
      const promises = [];


      res.forEach((item) => {
        promises.push(remove(urls.invoiceItems(invoice.id, item.id)));
      });

      Promise.all(promises).finally(() => {
        const itemPromises = [];

        invoiceItems.forEach((item) => {
          itemPromises.push(post(urls.invoiceItems(invoice.id),
            {
              invoice_id: item.invoice_id,
              product_id: item.product_id,
              quantity: item.quantity
            }));
        });
        Promise.all(itemPromises);
      });
    });
  });
};

export const getInvoices = () => (dispatch) => {
  get(urls.invoices()).then(res => dispatch({
    type: GET_INVOICES,
    value: res
  }));
};

export const sortByTotal = () => ({
  type: SORT_BY_TOTAL
});

export const sortByTotalReverse = () => ({
  type: SORT_BY_TOTAL_REVERSE
});
