import {
  ADD_TODO,
  DELETE_TODO,
  CHANGE_TODO_STATUS,
  CHANGE_TODO_NAME,
  DELETE_ALL_DONE,
  COMPLETE_ALL_TASKS
} from "./actionTypes";

export const addTodo = value => ({
  type: ADD_TODO,
  value
});

export const deleteTodo = value => ({
  type: DELETE_TODO,
  value

});

export const changeTodoStatus = value => ({
  type: CHANGE_TODO_STATUS,
  value
});

export const changeName = value => ({
  type: CHANGE_TODO_NAME,
  value
});

export const deleteAllDone = () => ({
  type: DELETE_ALL_DONE
});

export const completeAllTasks = () => (
  {
    type: COMPLETE_ALL_TASKS
  }
);
