import {
  LOG_IN,
  LOG_OUT,
  CREATE_NEW_USER, UPLOAD_IMAGE
} from "./actionTypes";

import urls from "../../utils/url";
import { post, put } from "../../utils/api";

export const logIn = user => (dispatch) => {
  post(urls.userAuth, user).then((res) => {
    dispatch({
      type: LOG_IN,
      value: res
    });
  });
  //   dispatch({
  //   type: LOG_IN,
  //   value: res
  // }));
};

export const logOut = () => ({
  type: LOG_OUT
});

export const registration = user => (dispatch) => {
  post(urls.userReg, user).then((res) => {
    dispatch({
      type: LOG_IN,
      value: res
    });
  });
};

export const updateUserData = user => (dispatch) => {
  put(urls.userData(user.id), user).then((res) => {
    dispatch({
      type: LOG_IN,
      value: res
    });
  });
};

export const updateUserPassword = data => (dispatch) => {
  put(urls.userPassword(data.id), data).then(() => {
    dispatch({
      type: LOG_OUT
    });
  });
};

export const uploadImage = value => ({
  type: UPLOAD_IMAGE,
  value
});
