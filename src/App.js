import React, { Component } from "react";
import { Route, Switch } from "react-router";
import { connect } from "react-redux";
import MainHeader from "./components/MainHeader";
import Registration from "./pages/Registration";

import TodoList from "./pages/MainComponentTodo";
import MainComponentInvoices from "./pages/MainComponentInvoices";
import MainPage from "./pages/MainPage";
import Authorization from "./pages/Authorization";
import UserPage from "./pages/UserPage";


class App extends Component {
  constructor(props) {
    super(props);
  }

  checkLogIn = () => {
    const { userData, location } = this.props;

    if (userData.isLogIn === true) {
      return (
        <div className="main-route">
          <MainHeader location={location} />
          <Switch>
            <Route exact path="/" component={MainPage} />
            <Route path="/todolist" component={TodoList} />
            <Route path="/invoices" component={MainComponentInvoices} />
            <Route path="/user" component={UserPage} />
          </Switch>
        </div>
      );
    }

    return (
      <div className="main-route">
        <Switch>
          <Route exact path="/" component={Authorization} />
          <Route path="/registration" component={Registration} />
        </Switch>
      </div>
    );
  };

  render() {
    return (
      this.checkLogIn()
    );
  }
}

function mapStateToProps(state) {
  return {
    userData: state.usersReducer
  };
}


export default connect(mapStateToProps)(App);
