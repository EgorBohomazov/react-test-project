import React from "react";
import { Link } from "react-router-dom";


const MainPage = () => (
  <div className="main-page-container">
    <div className="my-apps">
      <div className="list-group">
        <Link to="/todolist" className="list-group-item list-group-item-action flex-column align-items-start">
          <div className="d-flex w-100 justify-content-between">
            <h5 className="mb-1">TodoList application</h5>
            <small>Completed</small>
          </div>
          <div className="mb-1">
            <h4>Requirements:</h4>
            <ul>
              <li>Create todo</li>
              <li>Edit todo</li>
              <li>Complete todo;</li>
              <li>Remove todo</li>

            </ul>
          </div>
          <small>click here to check this app.</small>
        </Link>
        <Link to="/invoices" className="list-group-item list-group-item-action flex-column align-items-start">
          <div className="d-flex w-100 justify-content-between">
            <h5 className="mb-1">Implement CRUD for Invoices.</h5>
            <small>In progress</small>
          </div>
          <div className="mb-1">
            <h4>Requirements:</h4>
            <ul>
              <li>See list of Invoices</li>
              <li>Be able to Create / Edit / Delete Invoices</li>
              <li>In Invoice create / edit form I want to be able to:</li>
              <ul>
                <li>Select 1 Customer for Invoice.</li>
                <li>Add any number of products to Invoice.</li>
                <li>Specify quantity for each product.</li>
                <li>See price for each product item.</li>
                <li>Specify discount percentage for invoice total price.</li>
                <li>See Invoice total price with and without discount.</li>
              </ul>

            </ul>
          </div>
          <small>click here to check this app.</small>
        </Link>
      </div>
    </div>
  </div>
);

export default MainPage;
