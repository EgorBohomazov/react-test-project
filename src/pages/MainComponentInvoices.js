import React from "react";
import { Route, Switch } from "react-router";
import InvoicesMain from "../components/InvoicesComponents/Invoices/InvoicesMain";
import CustomersMain from "../components/InvoicesComponents/Customers/CustomersMain";
import ProductsMain from "../components/InvoicesComponents/Products/ProductsMain";
import ChartsMain from "../components/charts/ChartsMain";
import Navigation from "../components/InvoicesComponents/Navigation";

const MainComponentInvoices = () => (
  <div className="invoices-main-container">
    <Navigation />
    <Switch>
      <Route exact path="/invoices" component={InvoicesMain} />
      <Route path="/invoices/customers" component={CustomersMain} />
      <Route path="/invoices/products" component={ProductsMain} />
      <Route path="/invoices/charts" component={ChartsMain} />
    </Switch>
  </div>
);

export default MainComponentInvoices;
