import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { logIn } from "../actions/usersActions";


class Authorization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputLoginValue: "",
      inputPasswordValue: ""
    };
  }

  render() {
    return (
      <div className="main-route">
        <div className="main-page-container">
          <div className="main-component-container d-flex flex-column align-items-center justify-content-center">
            <form className="reg-auth-form">
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Login</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter login"
                  onChange={(e) => { this.setState({ inputLoginValue: e.target.value }); }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input
                  type="password"
                  onChange={(e) => { this.setState({ inputPasswordValue: e.target.value }); }}
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                />
              </div>
              <input
                type="button"
                className="btn btn-primary"
                onClick={() => {
                  const { logIn } = this.props;
                  const { inputLoginValue, inputPasswordValue } = this.state;

                  console.log(this.state);

                  logIn({ login: inputLoginValue, password: inputPasswordValue });
                }}
                value="Log In"
              />
              <input
                type="button"
                onClick={() => { this.props.history.push("/registration"); }}
                className="btn btn-primary ml-4"
                value="Registration"
              />
            </form>
          </div>
        </div>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
  logIn
}, dispatch);

function mapStateToProps(state) {
  return {
    userData: state.usersReducer
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Authorization);
