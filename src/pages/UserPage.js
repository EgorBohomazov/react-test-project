import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateUserData, updateUserPassword, logOut, uploadImage } from "../actions/usersActions";

class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputPasswordValue: "",
      inputNewPasswordValue: "",
      inputRepeatPasswordValue: "",
      inputFirstNameValue: props.userData.firstName,
      inputLastNameValue: props.userData.lastName,
      inputAddressValue: props.userData.address,
      inputPhoneValue: props.userData.phone
    };
  }


  onDrop = (picture) => {
    const { uploadImage } = this.props;

    uploadImage(URL.createObjectURL(picture));
  };

  userImageUrl = () => {
    const { userData } = this.props;

    if (userData.image.length === 0) {
      return "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png";
    }
    return userData.image;
  };


  render() {
    const { userData } = this.props;

    return (
      <div className="main-route">
        <div className="main-page-container">
          <div className="main-component-container d-flex flex-row align-items-center justify-content-center">
            <form className="reg-auth-form">
              <div className="form-group">
                <label
                  htmlFor="input-image"
                  data-toggle="tooltip"
                  data-placement="bottom"
                  title="Upload new image"
                >
                  <img width="120px" height="120px" src={this.userImageUrl()} alt="user" />
                </label>
                <input
                  className="image-upload"
                  id="input-image"
                  style={{ display: "none" }}
                  type="file"
                  onChange={(e) => { this.onDrop(e.target.files[0]); }}
                />
              </div>
              <div className="form-group ">
                <label>First Name</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter First Name"
                  value={this.state.inputFirstNameValue}
                  onChange={(e) => { this.setState({ inputFirstNameValue: e.target.value }); }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Last Name</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter Last Name"
                  value={this.state.inputLastNameValue}
                  onChange={(e) => { this.setState({ inputLastNameValue: e.target.value }); }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Address</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter address"
                  value={this.state.inputAddressValue}
                  onChange={(e) => { this.setState({ inputAddressValue: e.target.value }); }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Phone</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter phone"
                  value={this.state.inputPhoneValue}
                  onChange={(e) => { this.setState({ inputPhoneValue: e.target.value }); }}
                />
              </div>
              <input
                type="button"
                className="btn btn-primary"
                onClick={(e) => {
                  const fieldsRegEx = /^[a-zA-Z]+$/;
                  const phoneRegEx = /^\+380\d+$/;
                  const addressRegEx = /^[A-Za-z\s\d]+$/;
                  const {
                    inputFirstNameValue,
                    inputLastNameValue,
                    inputAddressValue,
                    inputPhoneValue
                  } = this.state;
                  const { history, updateUserData, userData } = this.props;

                  if (
                    fieldsRegEx.test(inputFirstNameValue) === true
                    && fieldsRegEx.test(inputLastNameValue) === true
                    && phoneRegEx.test(inputPhoneValue) === true
                    && addressRegEx.test(inputAddressValue) === true

                  ) {
                    updateUserData({
                      id: userData.id,
                      firstName: inputFirstNameValue,
                      lastName: inputLastNameValue,
                      address: inputAddressValue,
                      phone: inputPhoneValue
                    });
                    history.push("/");
                  } else {
                    e.preventDefault();
                  }
                }}
                value="Save Changes"
              />
            </form>
            <form className="reg-auth-form change-password">
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Previous password</label>
                <input
                  type="password"
                  onChange={(e) => { this.setState({ inputPasswordValue: e.target.value }); }}
                  className="form-control"
                  id="exampleInputPassword1"
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">New password</label>
                <input
                  type="password"
                  onChange={(e) => { this.setState({ inputNewPasswordValue: e.target.value }); }}
                  className="form-control"
                  id="exampleInputPassword1"
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Repeat new password</label>
                <input
                  type="password"
                  onChange={(e) => { this.setState({ inputRepeatPasswordValue: e.target.value }); }}
                  className="form-control"
                  id="exampleInputPassword1"
                />
              </div>
              <input
                type="button"
                className="btn btn-primary"
                value="Change password"
                onClick={(e) => {
                  const passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
                  const { inputPasswordValue, inputNewPasswordValue, inputRepeatPasswordValue } = this.state;
                  const { updateUserPassword, userData, history, logOut } = this.props;

                  if (inputNewPasswordValue === inputRepeatPasswordValue
                    && passwordRegEx.test(inputNewPasswordValue) === true

                  ) {
                    updateUserPassword({
                      id: userData.id,
                      oldPassword: inputPasswordValue,
                      password: inputNewPasswordValue });
                    logOut();
                    history.push("/");
                  } else {
                    e.preventDefault();
                  }
                }
              }
              />
            </form>
          </div>
        </div>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
  updateUserData,
  updateUserPassword,
  logOut,
  uploadImage
}, dispatch);

function mapStateToProps(state) {
  return {
    userData: state.usersReducer
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
