import React, { useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import TodosList from "../components/TodoList components/todosList";
import SortFooter from "../components/TodoList components/sortFooter";
import InputForm from "../components/TodoList components/inputForm";

const MainComponentTodo = (props) => {
  const { todoList } = props;
  const [filter, setFilter] = useState("all");

  function changeFilter(changedFilter) {
    setFilter(changedFilter);
  }

  function filterData() {
    if (filter === "all") {
      return todoList;
    }
    if (filter === "done") {
      return todoList.filter(elem => elem.status === true);
    }
    return todoList.filter(elem => elem.status === false);
  }

  return (
    <div className="todo-list-app">
      <div className="main-todo-block">
        <div className="todo-list-header">
          <h3>Todo List</h3>
          <InputForm />
        </div>
        <TodosList list={filterData()} />
        <SortFooter filter={filter} changeFilter={changeFilter} />
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    todoList: state.todoReducer
  };
}

MainComponentTodo.propTypes = {
  todoList: PropTypes.arrayOf(PropTypes.object)
};

MainComponentTodo.defaultProps = {
  todoList: []
};


export default connect(mapStateToProps)(MainComponentTodo);
