import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { registration } from "../actions/usersActions";


class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputLoginValue: "",
      inputPasswordValue: "",
      inputFirstNameValue: "",
      inputLastNameValue: "",
      inputAddressValue: "",
      inputPhoneValue: ""
    };
  }

  render() {
    return (
      <div className="main-route">
        <div className="main-page-container">
          <div className="main-component-container d-flex flex-column align-items-center justify-content-center">
            <form className="reg-auth-form">
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Login</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter login"
                  onChange={(e) => { this.setState({ inputLoginValue: e.target.value }); }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input
                  type="password"
                  onChange={(e) => { this.setState({ inputPasswordValue: e.target.value }); }}
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">First Name</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter First Name"
                  onChange={(e) => { this.setState({ inputFirstNameValue: e.target.value }); }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Last Name</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter Last Name"
                  onChange={(e) => { this.setState({ inputLastNameValue: e.target.value }); }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Address</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter address"
                  onChange={(e) => { this.setState({ inputAddressValue: e.target.value }); }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Phone</label>
                <input
                  type="login"
                  className="form-control"
                  aria-describedby="emailHelp"
                  placeholder="Enter phone"
                  onChange={(e) => { this.setState({ inputPhoneValue: e.target.value }); }}
                />
              </div>
              <input
                type="button"
                className="btn btn-primary"
                onClick={(e) => {
                  const passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
                  const fieldsRegEx = /^[a-zA-Z]+$/;
                  const phoneRegEx = /^\+380\d+$/;
                  const addressRegEx = /^[A-Za-z\s\d]+$/;
                  const {
                    inputLoginValue,
                    inputPasswordValue,
                    inputFirstNameValue,
                    inputLastNameValue,
                    inputAddressValue,
                    inputPhoneValue
                  } = this.state;
                  const { history, registration } = this.props;

                  if (fieldsRegEx.test(inputLoginValue) === true
                    && fieldsRegEx.test(inputFirstNameValue) === true
                    && fieldsRegEx.test(inputLastNameValue) === true
                    && passwordRegEx.test(inputPasswordValue) === true
                    && phoneRegEx.test(inputPhoneValue) === true
                    && addressRegEx.test(inputAddressValue) === true

                  ) {
                    registration({
                      login: inputLoginValue,
                      password: inputPasswordValue,
                      firstName: inputFirstNameValue,
                      lastName: inputLastNameValue,
                      address: inputAddressValue,
                      phone: inputPhoneValue
                    });
                    history.push("/");
                  } else {
                    e.preventDefault();
                  }
                }}
                value="Create account"
              />
              <input
                type="button"
                onClick={() => { this.props.history.push("/"); }}
                className="btn btn-primary ml-4"
                value="Log In"
              />
            </form>
          </div>
        </div>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
  registration
}, dispatch);

function mapStateToProps(state) {
  return {
    userData: state.usersReducer
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
