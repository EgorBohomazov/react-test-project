import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import CustomersChart from "./CustomersChart";
import { getInvoices } from "../../actions/invoicesActions";
import { getCustomers } from "../../actions/customersActions";
import { getProducts } from "../../actions/productsActions";
import { getAllInvoiceItems, clearInvoiceItems } from "../../actions/invoiceItemsActions";
import ProductsChart from "./ProductsChart";
import InvoicesChart from "./InvoicesChart";
import CustomersProductsChart from "./CustomersProductsChart";


class ChartsMain extends Component {
  constructor(props) {
    super(props);

    this.state = {
      customersData: [],
      productsData: [],
      invoicesData: [],
      productsCustomerData: []
    };
  }

  componentDidMount() {
    this.props.clearInvoiceItems();
    this.props.getCustomers()
      .then(() => this.props.getInvoices())
      .then(() => this.props.getProducts())
      .then(() => this.props.getAllInvoiceItems())
      .then(() => {
        this.setState({ customersData: this.formCustomerData(this.props.customersList, this.props.invoicesList) });
      });
  }


  formInvoicesData = (invoicesList, customersList) => {
    const data = [];

    invoicesList.forEach((invoice) => {
      customersList.forEach((customer) => {
        if (invoice.customer_id === customer.id) {
          data.push(
            { customerName: customer.name,
              Date: `${new Date(invoice.createdAt).getDay()}.${new Date(invoice.createdAt).getMonth()}.${new Date(invoice.createdAt).getFullYear()}`,
              total: invoice.total
            }
          );
        }
      });
    });

    return data;
  };

  productsChartBuild = (customerId) => {
    const { invoicesList, invoiceItemsList, productsArray } = this.props;
    const customerInvoiceItems = [];
    const customerProductsItem = [];
    const data = {
      labels: [],
      data: []
    };

    this.setState({ productsCustomerId: customerId });
    const customersInvoices = invoicesList.filter(invoice => invoice.customer_id === customerId);

    customersInvoices.forEach((invoice) => {
      invoiceItemsList.forEach((invoiceItem) => {
        if (invoice.id === invoiceItem.invoice_id) {
          customerInvoiceItems.push(invoiceItem);
        }
      });
    });

    customerInvoiceItems.forEach((invoiceItem) => {
      productsArray.forEach((product) => {
        if (invoiceItem.product_id === product.id) {
          customerProductsItem.push({ productName: product.name, quantity: invoiceItem.quantity });
        }
      });
    });

    customerProductsItem.forEach((item) => {
      data.labels.push(item.productName);
      data.data.push(item.quantity);
    });

    this.setState({ productsCustomerData: data });
  };

  formCustomerData = (customersList, invoicesList) => {
    const data = [];

    customersList.forEach((customer) => {
      data.push({ customerName: customer.name, customerId: customer.id, invoicesList: [] });
    });

    data.forEach((customerDataItem) => {
      invoicesList.forEach((invoice) => {
        if (customerDataItem.customerId === invoice.customer_id) {
          customerDataItem.invoicesList.push(invoice.total);
        }
      });
    });
    return data;
  };

  formProductsData = (productsList, invoiceItems) => {
    const labels = [];
    const data = [];


    productsList.forEach((product) => {
      invoiceItems.forEach((invoiceItem) => {
        if (product.id === invoiceItem.product_id) {
          data.push(invoiceItem.quantity);
          labels.push(`${product.name}, ${product.price} $`);
        }
      });
    });

    return { productLabels: labels, productQuants: data };
  };

  render() {
    const { productsArray, invoiceItemsList, customersList, invoicesList } = this.props;
    const { customersData, productsCustomerData } = this.state;


    return (
      <div className="main-block">
        <div className="main-component-container">
          <div className="d-flex flex-wrap">
            <CustomersChart
              data={customersData}
              sendCustomerId={this.productsChartBuild}
            />
            <CustomersProductsChart data={productsCustomerData} />
            <ProductsChart
              data={this.formProductsData(productsArray, invoiceItemsList)}
            />
            <InvoicesChart data={this.formInvoicesData(invoicesList, customersList)} />
          </div>
        </div>
      </div>
    );
  }
}

// function mapStateToProps(state) {
//   return {
//     productsList: state.productsReducer
//   };
// }
//
//
ChartsMain.propTypes = {
  invoicesList: PropTypes.arrayOf(PropTypes.object),
  customersList: PropTypes.arrayOf(PropTypes.object),
  productsArray: PropTypes.arrayOf(PropTypes.object),
  invoiceItemsList: PropTypes.arrayOf(PropTypes.object),
  getCustomers: PropTypes.func,
  getInvoices: PropTypes.func,
  getProducts: PropTypes.func

};

ChartsMain.defaultProps = {
  invoicesList: [],
  customersList: [],
  productsArray: [],
  invoiceItemsList: [],
  getCustomers: () => {},
  getInvoices: () => {},
  getProducts: () => {}
};

function mapStateToProps(state) {
  return {
    customersList: state.customersReducer,
    invoicesList: state.invoicesReducer,
    productsArray: state.productsReducer,
    invoiceItemsList: state.invoiceItemsReducer
  };
}
//
const mapDispatchToProps = dispatch => bindActionCreators({
  getInvoices,
  getCustomers,
  getProducts,
  getAllInvoiceItems,
  clearInvoiceItems
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(ChartsMain);
