import React, { Component } from "react";
import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Text, Rectangle
} from "recharts";

export default class CustomersChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      customersData: [],
      productsData: [],
      invoicesData: []
    };
  }

  static jsfiddleUrl = "https://jsfiddle.net/alidingling/9hjfkp73/";

  getBarsData(data) {
    const barsData = [];
    const invoices = [];

    data.forEach((item) => {
      const customer = {};

      customer.customerName = item.customerName;
      customer.customerId = item.customerId;

      item.invoicesList.forEach((invoice, index) => {
        const invoiceKey = `invoce-${index}`;

        customer[`invoce-${index}`] = invoice;

        if (!~invoices.indexOf(invoiceKey)) {
          invoices.push(invoiceKey);
        }
      });

      barsData.push(customer);
    });

    return { barsData, invoices };
  }

  CustomYaxisLabel = ({ x, y, payload, index, visibleTicksCount, height }) => (
    <Text
      key={index.toString()}
      textAnchor="end"
      x={x}
      y={y}
      verticalAnchor="middle"
      fill="#666"
      index={index}
      visibleTicksCount={visibleTicksCount}
      scaleToFit={false}
      stroke="none"
      lineHeight="1em"
      height={height}
      className="recharts-cartesian-axis-tick-value"
      payload={payload}
    >
      {`$${payload.value}`}
    </Text>
  );

  CustomBar = (props) => {
    const { sendCustomerId } = this.props;

    return (<Rectangle {...props} onClick={() => sendCustomerId(props.customerId)} />);
  };

  render() {
    const { data } = this.props;
    const { barsData, invoices } = this.getBarsData(data);
    const colorArray = ["#FF6633", "#FFB399", "#FF33FF", "#FFFF99", "#00B3E6",
      "#E6B333", "#3366E6", "#999966", "#99FF99", "#B34D4D",
      "#80B300", "#809900", "#E6B3B3", "#6680B3", "#66991A",
      "#FF99E6", "#CCFF1A", "#FF1A66", "#E6331A", "#33FFCC",
      "#66994D", "#B366CC", "#4D8000", "#B33300", "#CC80CC",
      "#66664D", "#991AFF", "#E666FF", "#4DB3FF", "#1AB399",
      "#E666B3", "#33991A", "#CC9999", "#B3B31A", "#00E680",
      "#4D8066", "#809980", "#E6FF80", "#1AFF33", "#999933",
      "#FF3380", "#CCCC00", "#66E64D", "#4D80CC", "#9900B3",
      "#E64D66", "#4DB380", "#FF4D4D", "#99E6E6", "#6666FF"];


    return (
      <BarChart
        width={700}
        height={500}
        data={barsData}
        margin={{
          top: 20, right: 30, left: 20, bottom: 5
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="customerName" />
        <YAxis tick={this.CustomYaxisLabel} />
        <Tooltip />
        <Legend />
        { invoices.map((invoice, index) => (
          <Bar
            dataKey={invoice}
            stackId="a"
            fill={colorArray[index]}
            shape={this.CustomBar}
          />
        )) }
      </BarChart>
    );
  }
}
