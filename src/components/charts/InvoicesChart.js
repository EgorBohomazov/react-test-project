import React, { Component } from "react";
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Text
} from "recharts";

export default class InvoicesChart extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }


  static jsfiddleUrl = "https://jsfiddle.net/alidingling/xqjtetw0/";

  CustomTooltip = ({ payload, active }) => {
    if (active) {
      return (
        <div className="custom-tooltip">
          <p className="label">{`${payload[0].payload.customerName} : ${payload[0].value} $`}</p>
        </div>
      );
    }

    return null;
  };

  CustomYaxisLabel = ({ x, y, payload, index, visibleTicksCount, height }) => (
    <Text
      key={index.toString()}
      textAnchor="end"
      x={x}
      y={y}
      verticalAnchor="middle"
      fill="#666"
      index={index}
      visibleTicksCount={visibleTicksCount}
      scaleToFit={false}
      stroke="none"
      lineHeight="1em"
      height={height}
      className="recharts-cartesian-axis-tick-value"
      payload={payload}
    >
      {`$${payload.value}`}
    </Text>
  );

  render() {
    return (
      <LineChart
        width={500}
        height={300}
        data={this.props.data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="Date" />
        <YAxis tick={this.CustomYaxisLabel} />
        <Tooltip content={this.CustomTooltip} />
        <Legend />
        <Line type="monotone" dataKey="total" stroke="#8884d8" activeDot={{ r: 8 }} />
      </LineChart>
    );
  }
}
