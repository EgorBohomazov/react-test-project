import React, { useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { addUser } from "../../../actions/customersActions";


const AddUserForm = (props) => {
  const [inputName, setInputName] = useState("");
  const [inputAdress, setInputAdress] = useState("");
  const [inputPhone, setInputPhone] = useState("");

  const { closeWindow, addUser } = props;

  function sendUserToStore(e) {
    const nameReg = /^[a-zA-Z]+$/;
    const regAdress = /^[A-Za-z\s\d]+$/;
    const regNumber = /^\+380\d+$/;

    const newUser = {
      name: inputName,
      address: inputAdress,
      phone: inputPhone,
      checked: false
    };

    console.log(nameReg.test(newUser.name));
    console.log(regAdress.test(newUser.address));
    console.log(regNumber.test(newUser.phone));

    if (newUser.address.length > 0
        && newUser.phone.length > 0
        && newUser.address.length > 0
        && nameReg.test(newUser.name) === true
        && regAdress.test(newUser.address) === true
        && regNumber.test(newUser.phone) === true) {
      addUser(newUser);
      closeWindow();
    } else {
      e.preventDefault();
    }
  }

  return (
    <div className="modal-input-user-container">
      <form className="submit-user-form">
        <div className="form-group">
          <label>Name</label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            placeholder="Name"
            onChange={(e) => { setInputName(e.target.value); }}
          />
        </div>
        <div className="form-group">
          <label>Adress</label>
          <input
            type="text"
            className="form-control"
            placeholder="Adress"
            onChange={(e) => { setInputAdress(e.target.value); }}
          />
        </div>
        <div className="form-group">
          <label>Phone</label>
          <input
            type="text"
            className="form-control"
            placeholder="Phone"
            onChange={(e) => { setInputPhone(e.target.value); }}
          />
        </div>
        <input type="button" onClick={sendUserToStore} className="btn btn-primary" value="Create user" />
      </form>

      <input type="button" value="x" onClick={closeWindow} className="btn btn-primary close-modal-button" />
    </div>
  );
};

const mapDispatchToProps = dispatch => bindActionCreators({
  addUser
}, dispatch);


export default connect(null, mapDispatchToProps)(AddUserForm);
