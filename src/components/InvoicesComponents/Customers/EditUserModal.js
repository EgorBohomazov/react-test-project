import React, { useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { setUser } from "../../../actions/customersActions";


const EditUserForm = (props) => {
  const { closeWindow, customerName, customerAdress, customerPhone, customerId, setUser } = props;
  const [inputName, setInputName] = useState(customerName);
  const [inputAdress, setInputAdress] = useState(customerAdress);
  const [inputPhone, setInputPhone] = useState(customerPhone);

  function editUser(e) {
    const changedUser = {
      name: inputName,
      address: inputAdress,
      phone: inputPhone,
      id: customerId
    };

    if (changedUser.name.length > 0 && changedUser.phone.length > 0 && changedUser.address.length > 0) {
      setUser(changedUser);
      closeWindow();
    } else {
      e.preventDefault();
    }
  }


  return (
    <div className="modal-input-user-container">
      <form className="submit-user-form">
        <div className="form-group">
          <label>Name</label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            value={inputName}
            onChange={(e) => { setInputName(e.target.value); }}
          />
        </div>
        <div className="form-group">
          <label>Address</label>
          <input
            type="text"
            className="form-control"
            value={inputAdress}
            onChange={(e) => { setInputAdress(e.target.value); }}
          />
        </div>
        <div className="form-group">
          <label>Phone</label>
          <input
            type="text"
            className="form-control"
            value={inputPhone}
            onChange={(e) => { setInputPhone(e.target.value); }}
          />
        </div>
        <input type="button" onClick={editUser} className="btn btn-primary" value="Edit user" />
      </form>

      <input type="button" value="x" onClick={closeWindow} className="btn btn-primary close-modal-button" />
    </div>
  );
};

const mapDispatchToProps = dispatch => bindActionCreators({
  setUser
}, dispatch);


export default connect(null, mapDispatchToProps)(EditUserForm);
