import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AddUserForm from "./AddUserForm";
import CustomerItem from "./CustomerItem";

import CustomModal from "../../customModal";
import { checkAll, deleteChecked, getCustomers } from "../../../actions/customersActions";


const CustomersMain = (props) => {
  const [isOpenModal, setIsOpen] = useState(false);
  const { customersList, checkAll, deleteChecked } = props;

  useEffect(() => {
    props.getCustomers();
  }, []);


  const loading = () => {
    if (customersList.length !== 0) {
      return (
        customersList.map(customer => (
          <CustomerItem
            key={customer.id}
            name={customer.name}
            address={customer.address}
            phone={customer.phone}
            id={customer.id}
            checked={customer.checked}
          />
        )));
    }
    return (
      <div className="spinner-border text-primary m-4" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    );
  };

  return (
    <div className="main-block">
      <div className="main-component-container">
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">
                <input
                  type="checkbox"
                  checked={customersList.every(customer => customer.checked === true)}
                  onChange={(e) => { checkAll(e.target.checked); }}
                />
              </th>
              <th scope="col">Name</th>
              <th scope="col">Address</th>
              <th scope="col">Phone</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              loading()
              // customersList.map(customer => (
              //   <CustomerItem
              //     key={customer.id}
              //     name={customer.name}
              //     address={customer.address}
              //     phone={customer.phone}
              //     id={customer.id}
              //     checked={customer.checked}
              //   />
              }
          </tbody>
        </table>
        <input
          type="button"
          className="btn btn-primary add-user-button"
          value="Add User +"
          onClick={() => { setIsOpen(true); }}
        />
        <input
          type="button"
          className="btn btn-primary add-user-button"
          value="Delete"
          onClick={() => { deleteChecked(customersList); }}
        />
        <CustomModal ariaHideApp={false} isOpen={isOpenModal} className="modal-window" overlayClassName="modal-overlay">
          <AddUserForm closeWindow={() => { setIsOpen(false); }} />
        </CustomModal>
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    customersList: state.customersReducer
  };
}

const mapDispatchToProps = dispatch => bindActionCreators({
  checkAll,
  deleteChecked,
  getCustomers
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CustomersMain);
