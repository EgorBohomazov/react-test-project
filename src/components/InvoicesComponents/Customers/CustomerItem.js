import React, { useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import CustomModal from "../../customModal";
import EditUserModal from "./EditUserModal";

import { deleteUser, setChecked } from "../../../actions/customersActions";

const CustomerItem = (props) => {
  const { name, address, phone, id, deleteUser, setChecked, checked } = props;
  const [editModal, setEditModal] = useState(false);

  return (
    <tr>
      <th scope="row"><input onChange={() => { setChecked(id); }} type="checkbox" checked={checked} /></th>
      <td>{name}</td>
      <td>{address}</td>
      <td>{phone}</td>
      <td>
        <i className="fa fa-pencil edit-item" onClick={() => { setEditModal(true); }} />
        <i className="fa fa-times edit-item" onClick={() => deleteUser(id)} />
      </td>
      <CustomModal ariaHideApp={false} isOpen={editModal} className="modal-window" overlayClassName="modal-overlay">
        <EditUserModal
          closeWindow={() => { setEditModal(false); }}
          customerName={name}
          customerAdress={address}
          customerPhone={phone}
          customerId={id}
        />
      </CustomModal>
    </tr>

  );
};

// TodoListItem.propTypes = {
//   deleteTodo: PropTypes.func,
//   changeName: PropTypes.func,
//   changeTodoStatus: PropTypes.func,
//   id: PropTypes.string,
//   task: PropTypes.string,
//   status: PropTypes.bool
//
// };
//
// TodoListItem.defaultProps = {
//   deleteTodo: () => {},
//   changeName: () => {},
//   changeTodoStatus: () => {},
//   id: "",
//   task: "",
//   status: false
//
// };

// const mapDispatchToProps = dispatch => bindActionCreators({
//   deleteTodo,
//   changeTodoStatus,
//   changeName
// }, dispatch);

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteUser,
  setChecked
}, dispatch);


export default connect(null, mapDispatchToProps)(CustomerItem);
