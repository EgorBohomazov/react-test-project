import React, { useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { setProduct } from "../../../actions/productsActions";


const EditProductForm = (props) => {
  const { closeWindow, productName, productPrice, productId, setProduct } = props;
  const [inputProduct, setInputProduct] = useState(productName);
  const [inputPrice, setInputPrice] = useState(productPrice);

  function editProduct(e) {
    const changedProduct = {
      name: inputProduct,
      price: inputPrice,
      id: productId
    };
    // console.log(changedProduct.id);

    if (changedProduct.name.length > 0 && changedProduct.price > 0) {
      setProduct(changedProduct);
      closeWindow();
    } else {
      e.preventDefault();
    }
  }


  return (
    <div className="modal-input-user-container">
      <form className="submit-user-form">
        <div className="form-group">
          <label>Product Name</label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            value={inputProduct}
            onChange={(e) => { setInputProduct(e.target.value); }}
          />
        </div>
        <div className="form-group">
          <label>Price</label>
          <input
            type="text"
            className="form-control"
            value={inputPrice}
            onChange={(e) => { setInputPrice(e.target.value); }}
          />
        </div>

        <input type="button" onClick={editProduct} className="btn btn-primary" value="Edit product" />
      </form>

      <input type="button" value="x" onClick={closeWindow} className="btn btn-primary close-modal-button" />
    </div>
  );
};

const mapDispatchToProps = dispatch => bindActionCreators({
  setProduct
}, dispatch);


export default connect(null, mapDispatchToProps)(EditProductForm);
