import React, { useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { addProduct } from "../../../actions/productsActions";


const AddProductForm = (props) => {
  const { addProduct } = props;
  const [inputProduct, setInputProduct] = useState("");
  const [inputPrice, setInputPrice] = useState("");
  //
  const { closeWindow } = props;
  //

  function sendProductToStore(e) {
    const nameReg = /^[a-zA-Z]+$/;
    const newProduct = {
      name: inputProduct,
      price: inputPrice,
      checked: false
    };

    if (
      newProduct.name.length > 0
        && newProduct.price.length > 0
        && isNaN(parseFloat(newProduct.price)) === false
        && nameReg.test(newProduct.name) === true
    ) {
      addProduct(newProduct);
      closeWindow();
    } else {
      e.preventDefault();
    }
  }

  return (
    <div className="modal-input-user-container">
      <form className="submit-user-form">
        <div className="form-group">
          <label>Product Name</label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            placeholder="Product Name"
            onChange={(e) => { setInputProduct(e.target.value); }}
          />
        </div>
        <div className="form-group">
          <label>Price</label>
          <input
            type="text"
            className="form-control"
            placeholder="Price"
            onChange={(e) => { setInputPrice(e.target.value); }}
          />
        </div>
        <input type="button" className="btn btn-primary" value="Create product" onClick={sendProductToStore} />
      </form>

      <input type="button" value="x" onClick={closeWindow} className="btn btn-primary close-modal-button" />
    </div>
  );
};


const mapDispatchToProps = dispatch => bindActionCreators({
  addProduct
}, dispatch);


export default connect(null, mapDispatchToProps)(AddProductForm);
