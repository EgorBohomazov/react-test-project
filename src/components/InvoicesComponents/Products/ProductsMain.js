import React, { useState, useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import AddProductForm from "./AddProductForm";
import CustomModal from "../../customModal";
import ProductItem from "./ProductItem";
import { checkAllProducts, deleteCheckedProducts, getProducts } from "../../../actions/productsActions";
import CustomerItem from "../Customers/CustomerItem";


const ProductsMain = (props) => {
  const [isOpenModal, setIsOpen] = useState(false);
  const { productsList, checkAllProducts, deleteCheckedProducts } = props;

  useEffect(() => {
    props.getProducts();
  }, []);

  const loading = () => {
    if (productsList.length !== 0) {
      return (
        productsList.map(product => (
          <ProductItem
            key={product.id}
            name={product.name}
            price={product.price}
            id={product.id}
            checked={product.checked}
          />
        )));
    }
    return (
      <div className="spinner-border text-primary m-4" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    );
  };


  return (
    <div className="main-block">
      <div className="main-component-container">
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">
                <input
                  type="checkbox"
                  checked={productsList.every(elem => elem.checked === true)}
                  onChange={(e) => { checkAllProducts(e.target.checked); }}
                />
              </th>
              <th scope="col">Product</th>
              <th scope="col">Price</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {loading()}
          </tbody>
        </table>
        <input
          type="button"
          className="btn btn-primary add-user-button"
          value="Add Product +"
          onClick={() => { setIsOpen(true); }}
        />
        <input
          type="button"
          className="btn btn-primary add-user-button"
          value="Delete"
          onClick={() => { deleteCheckedProducts(productsList); }}
        />
        <CustomModal ariaHideApp={false} isOpen={isOpenModal} className="modal-window" overlayClassName="modal-overlay">
          <AddProductForm closeWindow={() => { setIsOpen(false); }} />
        </CustomModal>
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    productsList: state.productsReducer
  };
}

const mapDispatchToProps = dispatch => bindActionCreators({
  checkAllProducts,
  deleteCheckedProducts,
  getProducts
}, dispatch);


// const mapDispatchToProps = dispatch => bindActionCreators({
//   checkAll,
//   deleteChecked
// }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductsMain);
