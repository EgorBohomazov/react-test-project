import React, { useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import EditProductForm from "./EditProductForm";

import CustomModal from "../../customModal";
import { deleteProduct, setProductChecked } from "../../../actions/productsActions";

const ProductItem = (props) => {
  const { name, price, id, checked, deleteProduct, setProductChecked } = props;
  const [editModal, setEditModal] = useState(false);

  return (
    <tr>
      <th scope="row"><input checked={checked} type="checkbox" onChange={() => { setProductChecked(id); }} /></th>
      <td>{name}</td>
      <td>{`${price} $`}</td>
      <td>
        <i className="fa fa-pencil edit-item" onClick={() => { setEditModal(true); }} />
        <i className="fa fa-times edit-item" onClick={() => { deleteProduct(id); }} />
      </td>
      <CustomModal ariaHideApp={false} isOpen={editModal} className="modal-window" overlayClassName="modal-overlay">
        <EditProductForm
          closeWindow={() => { setEditModal(false); }}
          productName={name}
          productPrice={price}
          productId={id}
        />
      </CustomModal>
    </tr>

  );
};

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteProduct,
  setProductChecked
}, dispatch);


export default connect(null, mapDispatchToProps)(ProductItem);
