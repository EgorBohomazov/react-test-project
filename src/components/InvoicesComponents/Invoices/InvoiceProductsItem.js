import React, { useState, useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import PropTypes from "prop-types";
import CustomModal from "../../customModal";
import { deleteProduct, getProducts, setProductChecked } from "../../../actions/productsActions";
import { setQuantityInc, setQuantityDec } from "../../../actions/invoiceItemsActions";

const InvoiceProductsItem = (props) => {
  const {
    Product_id,
    quantity,
    list,
    deleteItem,
    price,
    checked,
    setQuantityInc,
    setQuantityDec,
    setItemChecked } = props;
  const [name, setName] = useState("");

  useEffect(() => {
    const product = list.find(elem => elem.id === +Product_id);

    setName(product.name);
  }, []);


  return (
    <tr>
      <th scope="row"><input checked={checked} onChange={() => { setItemChecked(Product_id); }} type="checkbox" /></th>
      <td>{name}</td>
      <td>
        <i
          className="fa fa-minus edit-item"
          onClick={(e) => {
            if (quantity > 1) {
              setQuantityDec(Product_id);
            } else {
              e.preventDefault();
            }
          }}
        />
        {quantity}
        <i
          className="fa fa-plus edit-item"
          onClick={(e) => {
            if (quantity < 40) {
              setQuantityInc(Product_id);
            } else {
              e.preventDefault();
            }
          }}
        />
      </td>
      <td>{`${+price.toFixed(2)} $`}</td>
      <td>
        <i className="fa fa-times edit-item" onClick={() => { deleteItem(Product_id); }} />
      </td>
    </tr>

  );
};

InvoiceProductsItem.propTypes = {
  price: PropTypes.number

};

InvoiceProductsItem.defaultProps = {
  price: 0

};


export default InvoiceProductsItem;
