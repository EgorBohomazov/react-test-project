import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";


class InvoiceProductInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productId: 0,
      productQuantity: 1,
      productPrice: 0
    };
  }


  QuantityInc = () => {
    const { productQuantity } = this.state;

    this.setState({ productQuantity: productQuantity + 1 });
  };

  QuantityDec = (e) => {
    const { productQuantity } = this.state;

    if (productQuantity > 0) {
      this.setState({ productQuantity: (productQuantity - 1) });
    } else {
      e.preventDefault();
    }
  };


  render() {
    const { productsList, sendItem } = this.props;
    const { productQuantity, productId, productPrice } = this.state;

    const productTotalPrice = productPrice * productQuantity;


    return (
      <div className="invoice-input-container">
        <div className="input-group user-choose">
          <div className="input-group-prepend">
            <label className="input-group-text" htmlFor="inputGroupSelect01">Product</label>
          </div>
          <select
            defaultValue="0"
            className="custom-select"
            id="inputGroupSelect01"
            onChange={(e) => {
              this.setState({ productId: +e.target.value });
              productsList.forEach((elem) => {
                if (elem.id === +e.target.value) {
                  this.setState({ productPrice: elem.price });
                }
              });
            }}
          >
            <option value="0">Choose..</option>
            {productsList.map(elem => <option key={elem.id} value={elem.id}>{elem.name}</option>)}

          </select>
        </div>
        <div className="product-counter">
          <i
            className="fa fa-minus product-counter-plus"
            aria-hidden="true"
            onClick={this.QuantityDec}
          />
          <span className="product-counter-count">{productQuantity}</span>
          <i
            className="fa fa-plus product-counter-minus"
            aria-hidden="true"
            onClick={this.QuantityInc}
          />

          <span className="product-counter-price">
            Price:
            {productTotalPrice.toFixed(2)}
            $
          </span>
        </div>
        <input
          type="button"
          className="btn btn-secondary add-product-button"
          value="Add Product"
          onClick={(e) => {
            if (productId !== 0) {
              sendItem({
                product_id: productId,
                quantity: productQuantity,
                price: productPrice * productQuantity,
                checked: false
              });
            } else {
              e.preventDefault();
            }
          }}
        />
      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    productsList: state.productsReducer
  };
}


InvoiceProductInput.propTypes = {
  productsList: PropTypes.arrayOf(PropTypes.object),
  sendItem: PropTypes.func

};

InvoiceProductInput.defaultProps = {
  productsList: [],
  sendItem: () => {}
};


export default connect(mapStateToProps)(InvoiceProductInput);
