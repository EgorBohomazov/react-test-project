import React, { useState } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { deleteInvoice, setInvoiceChecked } from "../../../actions/invoicesActions";

import CustomModal from "../../customModal";
import InvoiceEditForm from "./InvoiceEditForm";

const InvoiceItem = (props) => {
  const { total, discount, id, deleteInvoice, customerId, customersList, checked, setInvoiceChecked, date } = props;
  const [editModal, setEditModal] = useState(false);

  const customer = customersList.find(elem => elem.id === customerId);
  const customerName = customer ? customer.name : "";
  const createDate = new Date(date);

  return (
    <tr>
      <th scope="row"><input checked={checked} onChange={() => { setInvoiceChecked(id); }} type="checkbox" /></th>
      <td>
        {`
      ${createDate.getDay()} / 
      ${createDate.getMonth()} / 
      ${createDate.getFullYear()} 
      ${createDate.getHours()}:${createDate.getMinutes()}`}
      </td>
      <td>{customerName}</td>
      <td>{`${discount} %`}</td>
      <td>{`${total} $`}</td>
      <td>
        <i className="fa fa-pencil edit-item" onClick={() => { setEditModal(true); }} />
        <i className="fa fa-times edit-item" onClick={() => { deleteInvoice(id); }} />
      </td>
      <CustomModal
        ariaHideApp={false}
        isOpen={editModal}
        className="create-invoice-modal"
        overlayClassName="modal-overlay"
      >
        <InvoiceEditForm
          closeWindow={() => { setEditModal(false); }}
          total={total}
          invoiceId={id}
          discount={discount}
          customerId={customerId}
        />
      </CustomModal>
    </tr>

  );
};

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteInvoice,
  setInvoiceChecked
}, dispatch);

function mapStateToProps(state) {
  return {
    customersList: state.customersReducer
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(InvoiceItem);
