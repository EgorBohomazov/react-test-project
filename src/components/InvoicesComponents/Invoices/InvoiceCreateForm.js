import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import InvoiceProductInput from "./InvoiceProductInput";
import InvoiceProductsItem from "./InvoiceProductsItem";
import { addInvoice } from "../../../actions/invoicesActions";
import { getCustomers } from "../../../actions/customersActions";
import { getProducts } from "../../../actions/productsActions";

class InvoiceCreateForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      customer_id: 0,
      discount: 0,
      invoiceItems: []

    };
  }

  componentDidMount() {
    this.props.getCustomers();
    this.props.getProducts();

    // const promises = [];

    // Promise.all(promises).then()
  }


  getItem = (item) => {
    const { invoiceItems } = this.state;
    const checkSame = invoiceItems.some(elem => +elem.product_id === +item.product_id);

    if (checkSame === false) {
      const tempArr = invoiceItems;

      tempArr.push(item);
      this.setState({ invoiceItems: tempArr });
    } else {
      const tempArr = invoiceItems.map((elem) => {
        if (+elem.product_id === +item.product_id) {
          return { ...elem, quantity: elem.quantity + item.quantity, price: elem.price + item.price };
        }
        return elem;
      });

      this.setState({ invoiceItems: tempArr });
      // this.setState({ invoiceItems: invoiceItems.map((elem) => {
      //   if (elem.product_id === item.product_id) {
      //     return { ...elem, quantity: elem.quantity + item.quantity };
      //   }
      //   return elem;
      // }) });
    }
  };

  deleteItem = (id) => {
    const { invoiceItems } = this.state;

    this.setState({ invoiceItems: invoiceItems.filter(item => item.product_id !== id) });
  };


  culcTotal = () => {
    const { invoiceItems, discount } = this.state;
    const totalPrice = invoiceItems.reduce((sum, elem) => sum + elem.price, 0);

    return totalPrice - (discount / 100) * totalPrice;
  };

  discountInc = (e) => {
    const { discount } = this.state;

    if (discount < 25) {
      this.setState({ discount: discount + 1 });
    } else {
      e.preventDefault();
    }
  };

  discountDec = (e) => {
    const { discount } = this.state;

    if (discount > 0) {
      this.setState({ discount: discount - 1 });
    } else {
      e.preventDefault();
    }
  };

  setQuantityInc = (id) => {
    const { invoiceItems } = this.state;

    this.setState({ invoiceItems: invoiceItems.map((item) => {
      if (item.product_id === id) {
        return { ...item, quantity: item.quantity + 1, price: item.price + (item.quantity / item.price) };
      }
      return item;
    }) });
  };

  setQuantityDec = (id) => {
    const { invoiceItems } = this.state;

    this.setState({ invoiceItems: invoiceItems.map((item) => {
      if (item.product_id === id) {
        return { ...item, quantity: item.quantity - 1, price: item.price - (item.quantity / item.price) };
      }
      return item;
    }) });
  };

  setItemChecked = (id) => {
    const { invoiceItems } = this.state;

    this.setState({ invoiceItems: invoiceItems.map((item) => {
      if (item.product_id === id) {
        return { ...item, checked: !item.checked };
      }
      return item;
    })
    });
  };

  setItemsCheckedAll = (value) => {
    const { invoiceItems } = this.state;

    this.setState({ invoiceItems: invoiceItems.map(item => ({ ...item, checked: value })) });
  };

  deleteCheckedItems = () => {
    const { invoiceItems } = this.state;

    this.setState({ invoiceItems: invoiceItems.filter(item => item.checked === false) });
  };

  render() {
    const {
      customersList,
      productsArray,
      closeWindow,
      addInvoice
    } = this.props;
    const { discount, invoiceItems, customer_id } = this.state;

    return (
      <div className="invoice">
        <div className="modal-input-invoice-container">
          <div className="submit-invoice-form d-flex">
            <div className="input-invoice-main">
              <div className="input-group user-choose">
                <div className="input-group-prepend">
                  <label className="input-group-text" htmlFor="inputGroupSelect01">Customer</label>
                </div>
                <select
                  defaultValue="0"
                  className="custom-select"
                  id="inputGroupSelect01"
                  onChange={(e) => { this.setState({ customer_id: +e.target.value }); }}
                >
                  <option value="0">Choose...</option>
                  {customersList.map(elem => (
                    <option
                      key={elem.id}
                      value={elem.id}
                    >
                      {elem.name}
                    </option>
                  ))}
                </select>
              </div>
              <InvoiceProductInput className="products-list" sendItem={this.getItem} />
            </div>
            <div className="discount-total d-flex justify-content-around align-items-center">
              <div className="product-counter align-self-start">
                <i
                  className="fa fa-minus product-counter-plus"
                  aria-hidden="true"
                  onClick={this.discountDec}
                />
                <span className="product-counter-count">
                  {"Discount:"}
                  {" "}
                  { discount }
                  {" "}
                  {"%"}
                </span>
                <i
                  className="fa fa-plus product-counter-minus"
                  aria-hidden="true"
                  onClick={this.discountInc}
                />
              </div>

              <span className="product-counter-price total">
                total
                {" "}
                {this.culcTotal().toFixed(2)}
                {" "}
                    $
              </span>


            </div>


          </div>
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th scope="col">
                  <input
                    type="checkbox"
                    onChange={(e) => { this.setItemsCheckedAll(e.target.checked); }}
                    checked={invoiceItems.every(item => item.checked === true)}
                  />
                </th>
                <th scope="col">Product</th>
                <th scope="col">Quantity</th>
                <th scope="col">Price</th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {invoiceItems.map(elem => (
                <InvoiceProductsItem
                  key={elem.product_id}
                  Product_id={elem.product_id}
                  quantity={elem.quantity}
                  price={+elem.price.toFixed(2)}
                  list={productsArray}
                  checked={elem.checked}
                  deleteItem={this.deleteItem}
                  setQuantityInc={this.setQuantityInc}
                  setQuantityDec={this.setQuantityDec}
                  setItemChecked={this.setItemChecked}
                />
              ))
              }
            </tbody>
          </table>
          <input type="button" value="x" onClick={closeWindow} className="btn btn-primary close-modal-button" />
        </div>
        <div className="w-100 d-flex justify-content-around">
          <input
            type="button"
            value="Save"
            className="btn btn-primary"
            onClick={(e) => {
              if (invoiceItems.length > 0 && customer_id !== 0) {
                addInvoice({
                  customer_id,
                  discount,
                  total: this.culcTotal().toFixed(2),
                  checked: false }, invoiceItems);
                closeWindow();
              } else {
                e.preventDefault();
              }
            }}
          />
          <input type="button" value="Delete" onClick={this.deleteCheckedItems} className="btn btn-secondary" />
        </div>
      </div>

    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
  addInvoice, getCustomers, getProducts
}, dispatch);


function mapStateToProps(state) {
  return {
    customersList: state.customersReducer,
    productsArray: state.productsReducer
  };
}

// InvoiceCreateForm.propTypes = {
//   invoiceItems: PropTypes.arrayOf(PropTypes.object)
//
// };
//
// InvoiceCreateForm.defaultProps = {
//   InvoiceItems: []
//
// };


// export default connect(null, mapDispatchToProps)(AddProductForm);

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceCreateForm);
