import React, { useState, useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import InvoiceCreateForm from "./InvoiceCreateForm";
import InvoiceItem from "./InvoiceItem";
import CustomModal from "../../customModal";
import { getInvoices, checkAllInvoices, deleteInvoicesChecked } from "../../../actions/invoicesActions";
import { getCustomers } from "../../../actions/customersActions";
import { getProducts } from "../../../actions/productsActions";
import CustomerItem from "../Customers/CustomerItem";

const InvoicesMain = (props) => {
  const [isOpenModal, setIsOpen] = useState(false);
  const [listSort, setListSort] = useState("Total");
  const { invoicesList, customersList, productsArray, checkAllInvoices, deleteInvoicesChecked } = props;

  useEffect(() => {
    props.getCustomers().then(() => props.getInvoices()).then(() => props.getProducts());
  }, []);


  const sortInvoices = () => {
    const { invoicesList } = props;

    if (listSort === "Total") {
      return invoicesList.sort((a, b) => a.total - b.total);
    } if (listSort === "Total-Reverse") {
      return invoicesList.sort((a, b) => b.total - a.total);
    } if (listSort === "Discount") {
      return invoicesList.sort((a, b) => a.discount - b.discount);
    } if (listSort === "Discount-Reverse") {
      return invoicesList.sort((a, b) => b.discount - a.discount);
    } if (listSort === "Date") {
      return invoicesList.sort((a, b) => Date.parse(a.createdAt) - Date.parse(b.createdAt));
    } if (listSort === "Date-Reverse") {
      return invoicesList.sort((a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt));
    } if (listSort === "Customer") {
      invoicesList.map((invoice) => {
        customersList.forEach((customer) => {
          if (invoice.customer_id === customer.id) {
            return invoice.customerName = customer.name;
          }
          return invoice;
        });
      });
      return invoicesList.sort((a, b) => {
        if (a.customerName > b.customerName) {
          return 1;
        }
        if (a.customerName < b.customerName) {
          return -1;
        }
        return 0;
      });
    } if (listSort === "Customer-Reverse") {
      invoicesList.map((invoice) => {
        customersList.forEach((customer) => {
          if (invoice.customer_id === customer.id) {
            return invoice.customerName = customer.name;
          }
          return invoice;
        });
      });
      return invoicesList.sort((a, b) => {
        if (a.customerName > b.customerName) {
          return -1;
        }
        if (a.customerName < b.customerName) {
          return 1;
        }
        return 0;
      });
    }
  };

  const filterClass = (filter, filterReverse) => {
    const classString = "fa fa-chevron-";

    if (listSort === filter) {
      return `${classString}down edit-item`;
    }
    if (listSort === filterReverse) {
      return `${classString}up edit-item`;
    }
    return `${classString}down edit-item`;
  };


  const loading = () => {
    if (invoicesList.length !== 0) {
      return (
        sortInvoices().map(invoice => (
          <InvoiceItem
            key={invoice.id}
            id={invoice.id}
            customerId={invoice.customer_id}
            discount={invoice.discount}
            total={invoice.total}
            list={customersList}
            productsArray={productsArray}
            checked={invoice.checked}
            date={invoice.createdAt}
          />
        )));
    }
    return (
      <div className="spinner-border text-primary m-4" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    );
  };

  return (
    <div className="main-block">
      <div className="main-component-container">
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">
                <input
                  type="checkbox"
                  checked={invoicesList.every(elem => elem.checked === true)}
                  onChange={(e) => { checkAllInvoices(e.target.checked); }}
                />
              </th>
              <th scope="col">
                <span className="d-flex">
                  <i
                    className={filterClass("Date", "Date-Reverse")}
                    onClick={() => {
                      if (listSort === "Date") {
                        setListSort("Date-Reverse");
                      } else {
                        setListSort("Date");
                      }
                    }}
                  />
                  <p>Date</p>
                </span>

              </th>
              <th scope="col">
                <span className="d-flex">
                  <i
                    className={filterClass("Customer", "Customer-Reverse")}
                    onClick={() => {
                      if (listSort === "Customer") {
                        setListSort("Customer-Reverse");
                      } else {
                        setListSort("Customer");
                      }
                    }}
                  />
                  <p>Customer</p>
                </span>
              </th>
              <th scope="col">
                <span className="d-flex">
                  <i
                    className={filterClass("Discount", "Discount-Reverse")}
                    onClick={() => {
                      if (listSort === "Discount") {
                        setListSort("Discount-Reverse");
                      } else {
                        setListSort("Discount");
                      }
                    }}
                  />
                  <p>Discount</p>
                </span>
              </th>
              <th scope="col">
                <span className="d-flex">
                  <i
                    className={filterClass("Total", "Total-Reverse")}
                    onClick={() => {
                      if (listSort === "Total") {
                        setListSort("Total-Reverse");
                      } else {
                        setListSort("Total");
                      }
                    }}
                  />
                  <p>Total</p>
                </span>
              </th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {loading()}
          </tbody>
        </table>
        <input
          type="button"
          className="btn btn-primary add-user-button"
          value="Create invoice"
          onClick={() => { setIsOpen(true); }}
        />
        <input
          type="button"
          className="btn btn-primary add-user-button"
          value="Delete"
          onClick={() => { deleteInvoicesChecked(invoicesList); }}
        />
        <CustomModal
          ariaHideApp={false}
          isOpen={isOpenModal}
          className="create-invoice-modal"
          overlayClassName="modal-overlay"
        >
          <InvoiceCreateForm closeWindow={() => { setIsOpen(false); }} />
        </CustomModal>
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    customersList: state.customersReducer,
    invoicesList: state.invoicesReducer,
    productsArray: state.productsReducer
  };
}
//
const mapDispatchToProps = dispatch => bindActionCreators({
  getInvoices,
  getCustomers,
  getProducts,
  checkAllInvoices,
  deleteInvoicesChecked
}, dispatch);


// const mapDispatchToProps = dispatch => bindActionCreators({
//   checkAll,
//   deleteChecked
// }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InvoicesMain);
