import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import InvoiceProductInput from "./InvoiceProductInput";
import InvoiceProductsItem from "./InvoiceProductsItem";
import { setInvoice } from "../../../actions/invoicesActions";
import { getCustomers } from "../../../actions/customersActions";
import { getProducts } from "../../../actions/productsActions";
import {
  setQuantityInc,
  setQuantityDec,
  getInvoiceItems,
  addItemPrices,
  deleteInvoiceItemLocal,
  addInvoiceItemLocal,
  setItemChecked,
  setItemsCheckedAll,
  deleteCheckedItems
} from "../../../actions/invoiceItemsActions";

class InvoiceEditForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      customer_id: props.customerId,
      discount: 0
    };
  }


  componentDidMount() {
    console.log(this.props.invoiceItems);
    this.props.getInvoiceItems(this.props.invoiceId).then(() => {
      this.props.productsArray.forEach((elem) => {
        this.props.addItemPrices(elem);
        this.setState({ discount: this.props.discount });
      });
    });
  }


  getItem = (item) => {
    const { addInvoiceItemLocal, invoiceId } = this.props;

    addInvoiceItemLocal({
      invoice_id: invoiceId,
      product_id: item.product_id,
      quantity: item.quantity,
      price: item.price
    });
  };


  culcTotal = () => {
    const { invoiceItems } = this.props;
    const { discount } = this.state;
    const totalPrice = invoiceItems.reduce((sum, elem) => sum + elem.price, 0);

    return totalPrice - (discount / 100) * totalPrice;
  };

  discountInc = (e) => {
    const { discount } = this.state;

    if (discount < 25) {
      this.setState({ discount: discount + 1 });
    } else {
      e.preventDefault();
    }
  };

  discountDec = (e) => {
    const { discount } = this.state;

    if (discount > 0) {
      this.setState({ discount: discount - 1 });
    } else {
      e.preventDefault();
    }
  };


  render() {
    const {
      invoiceId,
      customersList,
      productsArray,
      invoiceItems,
      closeWindow,
      setInvoice,
      customerId,
      deleteInvoiceItemLocal,
      setQuantityInc,
      setQuantityDec,
      setItemChecked,
      setItemsCheckedAll,
      deleteCheckedItems
    } = this.props;
    const { discount, customer_id } = this.state;

    return (
      <div className="invoice">
        <div className="modal-input-invoice-container">
          <div className="submit-invoice-form d-flex">
            <div className="input-invoice-main">
              <div className="input-group user-choose">
                <div className="input-group-prepend">
                  <label className="input-group-text" htmlFor="inputGroupSelect01">Customer</label>
                </div>
                <select
                  defaultValue={customerId}
                  className="custom-select"
                  id="inputGroupSelect01"
                  onChange={(e) => { this.setState({ customer_id: +e.target.value }); }}
                >
                  <option value="0">Choose...</option>
                  {customersList.map(elem => (
                    <option
                      key={elem.id}
                      value={elem.id}
                    >
                      {elem.name}
                    </option>
                  ))}
                </select>
              </div>
              <InvoiceProductInput className="products-list" sendItem={this.getItem} />
            </div>
            <div className="discount-total d-flex justify-content-around">
              <div className="product-counter">
                <i
                  className="fa fa-minus product-counter-plus"
                  aria-hidden="true"
                  onClick={this.discountDec}
                />
                <span className="product-counter-count">
                  {"Discount:"}
                  { discount }
                    %
                </span>
                <i
                  className="fa fa-plus product-counter-minus"
                  aria-hidden="true"
                  onClick={this.discountInc}
                />
              </div>

              <span className="product-counter-price total align-self-center">
                total
                {" "}
                {this.culcTotal().toFixed(2)}
                {" "}
                    $
              </span>


            </div>


          </div>
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th scope="col">
                  <input
                    type="checkbox"
                    checked={invoiceItems.every(elem => elem.checked === true)}
                    onChange={(e) => { setItemsCheckedAll(e.target.checked); }}
                  />
                </th>
                <th scope="col">Product</th>
                <th scope="col">Quantity</th>
                <th scope="col">Price</th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {invoiceItems.map(elem => (
                <InvoiceProductsItem
                  key={elem.product_id}
                  Product_id={elem.product_id}
                  quantity={elem.quantity}
                  price={elem.price}
                  checked={elem.checked}
                  list={productsArray}
                  deleteItem={deleteInvoiceItemLocal}
                  setQuantityInc={setQuantityInc}
                  setQuantityDec={setQuantityDec}
                  setItemChecked={setItemChecked}
                />
              ))
              }
            </tbody>
          </table>
          <input type="button" value="x" onClick={closeWindow} className="btn btn-primary close-modal-button" />
        </div>
        <div className="w-100 d-flex justify-content-around">
          <input
            type="button"
            value="Save"
            className="btn btn-primary"
            onClick={(e) => {
              if (invoiceItems.length > 0 && customer_id !== 0) {
                setInvoice({ id: invoiceId, customer_id, discount, total: this.culcTotal().toFixed(2) }, invoiceItems);
                closeWindow();
              } else {
                e.preventDefault();
              }
            }}
          />
          <input type="button" value="Delete" onClick={deleteCheckedItems} className="btn btn-secondary" />
        </div>
      </div>

    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
  getCustomers,
  getProducts,
  getInvoiceItems,
  addItemPrices,
  addInvoiceItemLocal,
  deleteInvoiceItemLocal,
  setInvoice,
  setQuantityInc,
  setQuantityDec,
  setItemChecked,
  setItemsCheckedAll,
  deleteCheckedItems
}, dispatch);


function mapStateToProps(state) {
  return {
    customersList: state.customersReducer,
    productsArray: state.productsReducer,
    invoiceItems: state.invoiceItemsReducer
  };
}


// export default connect(null, mapDispatchToProps)(AddProductForm);

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceEditForm);
