import React from "react";
import { Link } from "react-router-dom";


const Navigation = () => {
  const path = window.location.pathname;

  return (
    <nav className="navbar invoices-navigation bg-light">
      <ul className="navbar-nav list-group w-100">
        <li
          className={`nav-item list-group-item list-group-item-action ${path === "/invoices" && "active"}`}
        >
          <Link to="/invoices" className="nav-link text-dark">Invoices</Link>
        </li>
        <li
          className={`nav-item list-group-item list-group-item-action ${path === "/invoices/customers" && "active"}`}
        >
          <Link to="/invoices/customers" className="nav-link text-dark">Customers</Link>
        </li>
        <li
          className={`nav-item list-group-item list-group-item-action ${path === "/invoices/products" && "active"}`}
        >
          <Link to="/invoices/products" className="nav-link text-dark">Products</Link>
        </li>

        <li
          className={`nav-item list-group-item list-group-item-action ${path === "/invoices/charts" && "active"}`}
        >
          <Link to="/invoices/charts" className="nav-link text-dark">Dashboards</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
