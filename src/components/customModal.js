import React from "react";
import Modal from "react-modal";

const CustomModal = props => (<Modal {...props}>{props.children}</Modal>);

export default CustomModal;
