import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { logOut } from "../actions/usersActions";

const MainHeader = (props) => {
  const path = window.location.pathname;
  const {logOut} = props;

  return (
    <ul className="nav nav-pills main-route-header">
      <li className="nav-item">
        <Link to="/" className={`nav-link ${path === "/" && "active"}`}>MainPage</Link>
      </li>
      <li className="nav-item">
        <Link to="/todolist" className={`nav-link ${path.includes("/todolist") && "active"}`}>TodoList</Link>
      </li>
      <li className="nav-item">
        <Link to="/invoices" className={`nav-link ${path.includes("/invoices") && "active"}`}>Invoices</Link>
      </li>
      <li className="nav-item ml-auto mr-2">
        <Link to="/user" className="nav-link"><i className="fa fa-user" aria-hidden="true" /></Link>
      </li>
      <li className="nav-item mr-2">
        <Link to="/" className="nav-link"><i onClick={logOut} className="fa fa-sign-out" aria-hidden="true" /></Link>
      </li>
    </ul>
  );
};

const mapDispatchToProps = dispatch => bindActionCreators({
  logOut
}, dispatch);


export default connect(null, mapDispatchToProps)(MainHeader);
