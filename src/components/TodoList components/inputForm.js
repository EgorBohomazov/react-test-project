import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import PropTypes from "prop-types";
import { addTodo } from "../../actions/todoActions";

class InputForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: ""
    };
  }

   changeInputValue = (e) => {
     this.setState({ inputValue: e.target.value });
   };

  createTask = (inputValue) => {
    const { addTodo } = this.props;

    addTodo({ text: inputValue, id: (Math.random() * 10).toString(), status: false });
    this.setState({ inputValue: "" });
  };


  render() {
    const { inputValue } = this.state;

    return (
      <div className="todo-list-form">
        <input
          type="text"
          className="input-group-text create-todo-field"
          value={inputValue}
          onChange={this.changeInputValue}
        />
        <input
          type="button"
          className="btn btn-primary btn-sm w-100 create-todo-btn"
          onClick={() => this.createTask(inputValue)}
          value="Create new task"
        />
      </div>
    );
  }
}

InputForm.propTypes = {
  addTodo: PropTypes.func
};

InputForm.defaultProps = {
  addTodo: () => {}
};

const mapDispatchToProps = dispatch => bindActionCreators({
  addTodo
}, dispatch);


export default connect(null, mapDispatchToProps)(InputForm);
