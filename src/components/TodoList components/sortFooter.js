import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { deleteAllDone, completeAllTasks } from "../../actions/todoActions";

const SortFooter = (props) => {
  const { filter, changeFilter, deleteAllDone, completeAllTasks } = props;

  const isAll = () => {
    const classString = "btn btn-primary btn-sm";

    if (filter === "all") {
      return `${classString} active`;
    } return classString;
  };


  const isDone = () => {
    const classString = "btn btn-primary btn-sm";

    if (filter === "done") {
      return `${classString} active`;
    } return classString;
  };

  const isLeft = () => {
    const classString = "btn btn-primary btn-sm";


    if (filter === "left") {
      return `${classString} active`;
    } return classString;
  };


  return (
    <div className="todo-list-sort">
      <div className="filter-buttons">
        <input
          className={isAll()}
          type="button"
          value="All tasks"
          onClick={() => changeFilter("all")}
        />
        <input
          className={isDone()}
          type="button"
          value="Completed tasks"
          onClick={() => changeFilter("done")}
        />
        <input
          className={isLeft()}
          type="button"
          value="Left to complete"
          onClick={() => changeFilter("left")}
        />
      </div>
      <div className="set-buttons">
        <input
          className="btn btn-secondary btn-sm"
          type="button"
          value="Delete completed tasks"
          onClick={() => { deleteAllDone(); }}
        />
        <input
          className="btn btn-secondary btn-sm"
          type="button"
          value="Complete all tasks"
          onClick={() => { completeAllTasks(); }}
        />
      </div>
    </div>
  );
};


SortFooter.propTypes = {
  filter: PropTypes.string,
  changeFilter: PropTypes.func,
  deleteAllDone: PropTypes.func,
  completeAllTasks: PropTypes.func

};

SortFooter.defaultProps = {
  filter: "",
  changeFilter: () => {},
  deleteAllDone: () => {},
  completeAllTasks: () => {}

};

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteAllDone,
  completeAllTasks
}, dispatch);

export default connect(null, mapDispatchToProps)(SortFooter);
