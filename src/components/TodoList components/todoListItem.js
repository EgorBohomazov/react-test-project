import React, { useState } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { deleteTodo, changeTodoStatus, changeName } from "../../actions/todoActions";

const TodoListItem = (props) => {
  const { task, status, id, changeName, deleteTodo, changeTodoStatus } = props;


  const [editMode, setEditMode] = useState(false);
  const [inputValue, setInputValue] = useState(task);

  const textDecoration = () => {
    if (status === true) {
      return "todo-list-item text-across";
    }
    return "todo-list-item";
  };


  const checkEditMode = () => {
    if (editMode === false) {
      return (<span onClick={() => changeTodoStatus(id)}>{task}</span>);
    }
    return (
      <input
        type="text"
        className="input-group-text change-name"
        value={inputValue}
        onChange={(e) => { setInputValue(e.target.value); }}
        onKeyDown={(e) => {
          if (e.keyCode === 13) {
            changeName(e);
          }
        }}
        onClick={(e) => { e.preventDefault(); }}
      />
    );
  };


  const checkButton = () => {
    if (editMode === false) {
      return (
        <input
          type="button"
          value="edit"
          className="btn btn-primary btn-sm todo-btn"
          onClick={() => setEditMode(!editMode)}
        />
      );
    }
    return (
      <span>
        <input
          type="button"
          value="change"
          onClick={() => {
            changeName({ id, text: inputValue });
            setEditMode(!editMode);
          }}
          className="btn btn-primary btn-sm todo-btn"
        />
        <input
          type="button"
          value="cancel"
          onClick={() => setEditMode(!editMode)}
          className="btn btn-primary btn-sm todo-btn"
        />
      </span>
    );
  };


  return (
    <li className={textDecoration()}>
      <div className="d-flex">
        <span>
          <input
            type="checkbox"
            onChange={() => { changeTodoStatus(id); }}
            checked={status}
          />
        </span>
        <span>{checkEditMode()}</span>
      </div>
      <div>
        <span>{checkButton()}</span>
        <input
          type="button"
          className="btn btn-secondary btn-sm todo-btn"
          value="x"
          onClick={() => { deleteTodo(id); }}
        />
      </div>
    </li>
  );
};

TodoListItem.propTypes = {
  deleteTodo: PropTypes.func,
  changeName: PropTypes.func,
  changeTodoStatus: PropTypes.func,
  id: PropTypes.string,
  task: PropTypes.string,
  status: PropTypes.bool

};

TodoListItem.defaultProps = {
  deleteTodo: () => {},
  changeName: () => {},
  changeTodoStatus: () => {},
  id: "",
  task: "",
  status: false

};

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteTodo,
  changeTodoStatus,
  changeName
}, dispatch);


export default connect(null, mapDispatchToProps)(TodoListItem);
