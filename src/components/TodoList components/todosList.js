import React from "react";
import PropTypes from "prop-types";
import TodoListItem from "./todoListItem";

const TodosList = (props) => {
  const { list } = props;


  return (
    <div className="todos-list-container">
      <ul className="todos-list">
        {list.map(elem => (
          <TodoListItem
            id={elem.id}
            key={elem.id}
            task={elem.text}
            status={elem.status}
            list={list}
          />
        ))}
      </ul>
    </div>
  );
};


TodosList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object)
};

TodosList.defaultProps = {
  list: []
};


export default TodosList;
