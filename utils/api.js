const headers = {
  "Content-Type": "application/json"
};

export const get = url => fetch(url).then(res => res.json());

export function post(url, data) {
  return fetch(url, { method: "POST", headers, body: JSON.stringify(data) }).then(res => res.json());
}

export function remove(url) {
  return fetch(url, { method: "DELETE" }).then(res => res.json());
}

export function put(url, data) {
  return fetch(url, { method: "PUT", headers, body: JSON.stringify(data) }).then(res => res.json());
}
