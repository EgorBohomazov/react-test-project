const URL_PREFIX = "http://localhost:8000/api";

const urls = {
  products: id => `${URL_PREFIX}/products${id ? `/${id}` : ""}`,
  customers: id => `${URL_PREFIX}/customers${id ? `/${id}` : ""}`,
  invoices: id => `${URL_PREFIX}/invoices${id ? `/${id}` : ""}`,
  invoiceItems: (invoiceId, id) => `${URL_PREFIX}/invoices/${invoiceId}/items${id ? `/${id}` : ""}`,
  userAuth: `${URL_PREFIX}/users/authorization`,
  userReg: `${URL_PREFIX}/users/registration`,
  userData: id => `${URL_PREFIX}/users/${id}`,
  userPassword: id => `${URL_PREFIX}/users/update/${id}`

};

export default urls;

// invoices: (id, itemId) => `${URL_PREFIX}/invoices${id ? `/${id}${itemId ? `/items/${itemId}` : ""}` : ""}`
